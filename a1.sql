
--Activity 1

INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", now()

);

INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", now()

);

INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", now()

);

INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", now()

);

INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", now()

);

-- add the following records to the blog_db database:

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", now()

);

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (2, "Second Code", "Hello Earth!", now()

);

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (3, "Third Code", "Welcome to Mars!", now()

);

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (4, "Fourth Code", "Bye bye solar system!", now()

);

-- Get all the post with an Author ID of 1.

SELECT * FROM posts
WHERE user_id = 1;

-- Get all the user's email and datetime of creation.

SELECT email, datetime_created FROM users;

-- Update a post's content to "Hello to the people of the earth!" where its initial content is "Hello Earth!" by using the record's ID.

UPDATE posts
SET content = "Hello to the people of the Earth!"
WHERE id = 2;

-- Delete the user with an email of "johndoe@gmail.com".

DELETE FROM users
WHERE email = "johndoe@gmail.com";